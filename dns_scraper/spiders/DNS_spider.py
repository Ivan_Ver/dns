import scrapy
from scrapy_splash import SplashRequest
from dns_scraper.items import Dns_product
from dns_scraper.util.util import Util


class DnsSpider(scrapy.Spider):
    name = 'default'
    dns_url = 'https://www.dns-shop.ru'
    lua_script = Util.read_lua_script()

    # custom_settings = {
    #     'DOWNLOAD_DELAY': 1,
    #     'DOWNLOAD_TIMEOUT': 10,
    #     # 'CONCURRENT_REQUESTS_PER_DOMAIN': 10,
    #     # 'CONCURRENT_REQUESTS_PER_IP': 10,
    #     'CONCURRENT_REQUESTS': 2
    # }

    def start_requests(self):
        yield self.get_request(
            url=self.dns_url + "/catalog/",
            callback=self.parse
        )

    def parse(self, response, **kwargs):
        first_level_links = response.xpath("//li[@class='subcategory__childs-list-item']")
        for i in range(len(first_level_links) - 4):
            # if i % 4 != 0:
            if i == 1:
                yield self.get_request(
                    url=self.dns_url + first_level_links[i].xpath("a/@href").get(),
                    callback=self.second_level_parse
                )

    def second_level_parse(self, response):
        table_links = response.xpath("//a[@class='ui-link']/@href").extract()
        if len(table_links) == 0:
            second_level_links = response.xpath("//div[@class='subcategory']//a/@href").extract()[0:1]
            for url in second_level_links:
                yield self.get_request(
                    url=self.dns_url + url,
                    callback=self.second_level_parse
                )
        else:
            yield self.get_request(
                url=response.url,
                callback=self.product_list_parser,
                cb_kwargs={'page': 1}
            )

    def product_list_parser(self, response, page):
        table_links = response.xpath("//div[@class='n-catalog-product__main']//a[@class='ui-link']/@href").extract()
        if len(table_links) != 0:
            for url in table_links:
                yield self.get_request(
                    url=self.dns_url + url,
                    callback=self.product_parser
                )
        else:
            return None
        page += 1
        yield self.get_request(
                url=response.url + "?p={}&order=1&groupBy=none&stock=2".format(page),
                callback=self.product_list_parser,
                cb_kwargs={'page': page}
            )

    @staticmethod
    def product_parser(response):
        product = Dns_product()
        product['url'] = response.url
        product['title'] = response.xpath("//h1[@class='page-title price-item-title']/text()").get()
        product['category'] = ">".join(response.xpath("//ol[@class='breadcrumb-list']//span/text()")
                                       .extract()[1:-2]).replace("\n", "")
        product['brand'] = response.xpath("//div[@class='brand-logo']/a/img/@alt").get()
        product['code'] = response.xpath("//div[@class='price-item-code']/span/text()").get()
        try:
            product['price'] = float((response.xpath("//span[@class='product-card-price__current']/text()")
                                      .get()).replace(" ", ""))
        except Exception:
            product['price'] = 0
        return product

    def get_request(self, url, callback, cb_kwargs=None):
        # return SplashRequest(
        #     url=url,
        #     callback=callback,
        #     endpoint='execute',
        #     cache_args=['lua_source'],
        #     args={
        #         'html': 1,
        #         'lua_source': self.lua_script
        #     },
        #     cb_kwargs=cb_kwargs,
        #     dont_filter=True)

        return scrapy.Request(
            url=url,
            callback=callback,
            cb_kwargs=cb_kwargs,
            dont_filter=True
        )
