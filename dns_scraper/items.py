# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class Dns_product(scrapy.Item):
    url = scrapy.Field()
    title = scrapy.Field()
    price = scrapy.Field()
    promotional_price = scrapy.Field()
    code = scrapy.Field()
    brand = scrapy.Field()
    category = scrapy.Field()

