# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from dns_scraper.util.database import Database


class DnsScraperPipeline:
    flush_count = 10
    items = list()

    def open_spider(self, spider):
        print("open")

    def close_spider(self, spider):
        print("over")

    def process_item(self, item, spider):
        self.items.append(item)
        return item
